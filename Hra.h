//
// Created by User on 19.12.2019.
//

#ifndef GAMECOMPANYMANAGER_HRA_H
#define GAMECOMPANYMANAGER_HRA_H

#include "Sklad.h"


class Hra {
private:
    int m_pocetKol;
    Sklad *m_sklad;

    void printStart();

    void printTah();

    void printInfo();

    void printVolby();

public:
    Hra();

    void hrej();

    ~Hra();


};


#endif //GAMECOMPANYMANAGER_HRA_H
