//
// Created by micha on 19.12.2019.
//

#ifndef GAMECOMPANYMANAGER_FACTORY_H
#define GAMECOMPANYMANAGER_FACTORY_H


#include "Stroj.h"
#include "Zbozi.h"
#include "Surovina.h"

class Factory {
public:
    Stroj *createStroj();

    Surovina *createSurovina(Jmena jmeno);

    Zbozi *createZbozi(Jmena jmeno, vector<PocetSurovin *> potrebneSuroviny);

};


#endif //GAMECOMPANYMANAGER_FACTORY_H
