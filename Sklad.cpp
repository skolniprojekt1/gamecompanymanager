//
// Created by micha on 21.11.2019.
//

#include "Sklad.h"

/*** Pomoci for cyklu postupne pridava vsechny suroviny z tovarny a vytvori prvni stroj ***/
Sklad::Sklad() {
    m_kredity = 50000;
    Factory tovarna;
    for (int i = Guma; i <= Hypermotor; i++) {
        m_suroviny.push_back(tovarna.createSurovina(Jmena(i)));
    }
    m_vyrobniStroje.push_back(tovarna.createStroj());
}

/*** Vytvori sklad a pomoci for cyklu pridava vsechny produkty (zbozi) z tovarne a pridava jim dvojice (struct - PocetSurovin.h)
 * surovina + mnozstvi, ktere v parametru ulozi kazdemu zbozi do vectoru m_zbozi ***/
Sklad *Sklad::createSklad() {
    Sklad *sklad = new Sklad();
    Factory tovarna;
    for (int i = BojovyDroidB1; i <= Devastator; i++) {
        vector<PocetSurovin *> pocetSurovin;
        switch (Jmena(i)) {
            case BojovyDroidB1:
                sklad->pridejSurovinu(Zelezo, 20, pocetSurovin);
                sklad->pridejSurovinu(Hlinik, 20, pocetSurovin);
                sklad->pridejSurovinu(Plastoid, 2, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case Droideka:
                sklad->pridejSurovinu(Zelezo, 100, pocetSurovin);
                sklad->pridejSurovinu(Guma, 20, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 5, pocetSurovin);
                sklad->pridejSurovinu(Karbonit, 1, pocetSurovin);
                sklad->pridejSurovinu(Med, 1, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case Stormtrooper:
                sklad->pridejSurovinu(Klon, 1, pocetSurovin);
                sklad->pridejSurovinu(Plastoid, 10, pocetSurovin);
                sklad->pridejSurovinu(Textil, 10, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case XWing:
                sklad->pridejSurovinu(Zelezo, 300, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 800, pocetSurovin);
                sklad->pridejSurovinu(Med, 50, pocetSurovin);
                sklad->pridejSurovinu(Plasma, 30, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case YWing:
                sklad->pridejSurovinu(Zelezo, 1000, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 500, pocetSurovin);
                sklad->pridejSurovinu(Med, 500, pocetSurovin);
                sklad->pridejSurovinu(Plasma, 20, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case TieFighter:
                sklad->pridejSurovinu(Zelezo, 500, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 900, pocetSurovin);
                sklad->pridejSurovinu(Med, 115, pocetSurovin);
                sklad->pridejSurovinu(Plasma, 24, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case SithStarfighter:
                sklad->pridejSurovinu(Zelezo, 1400, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 600, pocetSurovin);
                sklad->pridejSurovinu(Med, 500, pocetSurovin);
                sklad->pridejSurovinu(Plasma, 20, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case Jabitha:
                sklad->pridejSurovinu(Zelezo, 300, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 10000, pocetSurovin);
                sklad->pridejSurovinu(Med, 50, pocetSurovin);
                sklad->pridejSurovinu(Hypermotor, 1, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case MillenniumFalcon:
                sklad->pridejSurovinu(Zelezo, 1000, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 50000, pocetSurovin);
                sklad->pridejSurovinu(Med, 500, pocetSurovin);
                sklad->pridejSurovinu(Hypermotor, 1, pocetSurovin);
                sklad->pridejSurovinu(Plasma, 3160, pocetSurovin);
                sklad->pridejSurovinu(Karbonit, 50, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            case Devastator:
                sklad->pridejSurovinu(Zelezo, 200000, pocetSurovin);
                sklad->pridejSurovinu(Superocel, 900000, pocetSurovin);
                sklad->pridejSurovinu(Med, 100000, pocetSurovin);
                sklad->pridejSurovinu(Plasma, 380000, pocetSurovin);
                sklad->m_zbozi.push_back(tovarna.createZbozi(Jmena(i), pocetSurovin));
                break;
            default:
                break;
        }
    }
    return sklad;
}

int Sklad::getKredity() {
    return m_kredity;
}

/*** Prida pocty surovin a suroviny potrebne k vyrobe ke kazdemu zbozi (dvojice (struct PocetSurovin.h) do vektoru)***/
void Sklad::pridejSurovinu(Jmena jmeno, int mnozstvi, vector<PocetSurovin *> &pocetSurovin) {
    PocetSurovin *potrebneSuroviny = new PocetSurovin;
    potrebneSuroviny->pocet = mnozstvi;
    potrebneSuroviny->surovina = najdiSurovinu(jmeno);
    pocetSurovin.push_back(potrebneSuroviny);
}

int Sklad::getCelkoveMnozstviZbozi() {
    int pocet = 0;
    for (auto &zbozi : m_zbozi) {
        pocet += zbozi->getMnozstvi();
    }
    return pocet;
}

Surovina *Sklad::najdiSurovinu(Jmena jmeno) {
    for (auto &surovina : m_suroviny) {
        if (surovina->getJmeno() == jmeno) return surovina;
    }
    return nullptr;
}

Zbozi *Sklad::najdiZbozi(Jmena jmeno) {
    for (auto &zbozi : m_zbozi) {
        if (zbozi->getJmeno() == jmeno) return zbozi;
    }
    return nullptr;
}

vector<Zbozi *> Sklad::getDostupneZbozi() {
    vector<Zbozi *> dostupneZbozi;
    for (auto &zbozi : m_zbozi) {
        if (zbozi->getMnozstvi() > 0) dostupneZbozi.push_back(zbozi);
    }
    return dostupneZbozi;
}

bool Sklad::overDostatekSurovinKVyrobe(Zbozi *zbozi, int mnozstvi) {
    for (auto &pocetSurovin : zbozi->getPotrebneSuroviny()) {
        if (pocetSurovin->pocet * mnozstvi > pocetSurovin->surovina->getMnozstvi()) {
            cout << "Pozadovane mnozstvi zbozi nebylo mozne vyrobit." << endl;
            cout << "Na sklade mas pouze " << pocetSurovin->surovina->getMnozstvi() << " kg." << endl;
            cout << "K vyrobe je zapotrebi: " << pocetSurovin->pocet * mnozstvi << " kg "
                 << pocetSurovin->surovina->getNazev() << "." << endl;
            return false;
        }
    }
    return true;
}

bool Sklad::overDostatekVyrobnihoCasu(Zbozi *zbozi, int mnozstvi) {
    int celkovyCas = 0;
    for (auto &stroj : m_vyrobniStroje) celkovyCas += stroj->getVyrobniCas();
    if (zbozi->getVyrobniCas() * mnozstvi > celkovyCas) {
        cout << "Pozadovane mnozstvi zbozi nebylo mozne vyrobit." << endl;
        cout << "Celkovy strojni cas je " << celkovyCas << " h." << endl;
        cout << "K vyrobe je zapotrebi " << zbozi->getVyrobniCas() * mnozstvi << " h." << endl;
        return false;
    }
    return true;
}

bool Sklad::vyrobZbozi(Zbozi *zbozi, int mnozstvi) {
    if (!overDostatekSurovinKVyrobe(zbozi, mnozstvi) or !overDostatekVyrobnihoCasu(zbozi, mnozstvi)) return false;
    int potrebnyCas = zbozi->getVyrobniCas() * mnozstvi;
    int i = 0;
    while (potrebnyCas > 0 and i < m_vyrobniStroje.size()) {
        int casStroje = m_vyrobniStroje.at(i)->getVyrobniCas();
        if (potrebnyCas > casStroje) {
            potrebnyCas -= casStroje;
            m_vyrobniStroje.at(i)->snizCas(casStroje);
        } else {
            m_vyrobniStroje.at(i)->snizCas(potrebnyCas);
            potrebnyCas = 0;
        }
        i++;
    }

    for (auto &pocetSuroviny : zbozi->getPotrebneSuroviny()) {
        pocetSuroviny->surovina->snizMnozstvi(pocetSuroviny->pocet);
    }

    zbozi->zvysMnozstvi(mnozstvi);
    return true;
}

bool Sklad::nakupSurovinu(Surovina *surovina, int mnozstvi) {
    if (surovina->getCena() * mnozstvi > m_kredity) {
        cout << "Nedostatek kreditu." << endl;
        cout << "K dispozici je pouze " << m_kredity << "kreditu." << endl;
        cout << "K nakupu " << mnozstvi << " surovin je treba " << surovina->getCena() * mnozstvi << endl;
        return false;
    }
    odectiKredity(surovina->getCena() * mnozstvi);
    surovina->zvysMnozstvi(mnozstvi);
    return true;
}

bool Sklad::prodejSurovinu(Surovina *surovina, int mnozstvi) {
    return surovina->snizMnozstvi(mnozstvi);
}

bool Sklad::nakupStroj() {
    Factory tovarna;
    Stroj *stroj = tovarna.createStroj();
    if (stroj->getCena() <= m_kredity) {
        m_vyrobniStroje.push_back(stroj);
        odectiKredity(stroj->getCena());
        return true;
    }
    return false;
}

bool Sklad::prodejStroj() {
    if (m_vyrobniStroje.empty()) return false;
    Stroj *stroj = *(m_vyrobniStroje.end().operator--());
    prictiKredity(stroj->getCena() / 2);
    m_vyrobniStroje.pop_back();
    return true;
}

void Sklad::resetStroje() {
    for (auto &stroj : m_vyrobniStroje) {
        stroj->resetStroj();
    }
}

/*** Nahodna generace objednavek a nahodne odecitani zbozi podle dostupnosti ***/
void Sklad::zpracujObjednavky() {
    vector<Zbozi *> dostupneZbozi = getDostupneZbozi();
    if (!dostupneZbozi.empty()) {
        std::default_random_engine generator;
        std::uniform_int_distribution<int> rozmezi(0, getCelkoveMnozstviZbozi());
        int pocetObjednavek = rozmezi(generator);

        for (int i = 0; i < pocetObjednavek; i++) {
            dostupneZbozi = getDostupneZbozi();
            std::uniform_int_distribution<int> rozsahZbozi(0, dostupneZbozi.size() - 1);
            int nahodnyIndex = rozsahZbozi(generator);
            dostupneZbozi.at(nahodnyIndex)->snizMnozstvi(1);
            prictiKredity(dostupneZbozi.at(nahodnyIndex)->getCena());
        }
    }
}

bool Sklad::odectiPoplatky() {
    /*** Odectou se skladovaci poplatky a pokud neni dostatek kreditu, fuknce vrati false a funkce hrej() ukonci hru. ***/
    for (auto &zbozi : m_zbozi)
        if (!odectiKredity(zbozi->getSkladovaciPoplatek() * zbozi->getMnozstvi())) return false;
    for (auto &surovina : m_suroviny)
        if (!odectiKredity(surovina->getSkladovaciPoplatek() * surovina->getMnozstvi()))return false;
    for (auto &stroj : m_vyrobniStroje)
        if (!odectiKredity(stroj->getUdrzba())) return false;
    return true;
}


void Sklad::prictiKredity(int mnozstvi) {
    m_kredity += mnozstvi;
}

bool Sklad::odectiKredity(int mnozstvi) {
    if (mnozstvi > m_kredity) return false;
    m_kredity -= mnozstvi;
    return true;
}

void Sklad::printOddelovac() {
    cout << "__________________________" << endl;
}

void Sklad::printInfo() {
    printOddelovac();
    cout << "Kredity: " << m_kredity << endl;
    printOddelovac();
    for (auto &surovina : m_suroviny) {
        surovina->printInfo();
        printOddelovac();
    }
    for (auto &zbozi : m_zbozi) {
        zbozi->printInfo();
        printOddelovac();
    }
    for (auto &stroj : m_vyrobniStroje) {
        stroj->printInfo();
        printOddelovac();
    }
}

Sklad::~Sklad() {
    for (auto &zbozi : m_zbozi) delete zbozi;
    for (auto &surovina : m_suroviny) delete surovina;
    for (auto &stroj : m_vyrobniStroje) delete stroj;
}


