// Created by User on 18.12.2019.
//
#ifndef GAMECOMPANYMANAGER_ZBOZI_H
#define GAMECOMPANYMANAGER_ZBOZI_H

#include <vector>
#include <iostream>
#include "Surovina.h"
#include "PocetSurovin.h"
#include "Jmena.h"

using namespace std;

class Zbozi : public Surovina {
    vector<PocetSurovin *> m_potrebneSuroviny;
    int m_vyrobniCas;
public:
    Zbozi(string nazev, int cena, int skladovaciPoplatek, vector<PocetSurovin *> suroviny, int vyrobniCas, Jmena jmeno);

    vector<PocetSurovin *> getPotrebneSuroviny();

    int getVyrobniCas();

    void printInfo() override;

    ~Zbozi();
};


#endif //GAMECOMPANYMANAGER_ZBOZI_H
