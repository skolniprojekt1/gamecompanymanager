//
// Created by User on 09.11.2019.
//

#include "Surovina.h"

Surovina::Surovina(string nazev, int cena, int skladovaciPoplatek, Jmena jmeno) {
    m_nazev = nazev;
    m_cena = cena;
    m_skladovaciPoplatek = skladovaciPoplatek;
    m_mnozstvi = 0;
    m_jmeno = jmeno;
}

string Surovina::getNazev() {
    return m_nazev;
}

int Surovina::getCena() {
    return m_cena;
}

int Surovina::getSkladovaciPoplatek() {
    return m_skladovaciPoplatek;
}

int Surovina::getMnozstvi() {
    return m_mnozstvi;
}

Jmena Surovina::getJmeno() {
    return m_jmeno;
}

void Surovina::zvysMnozstvi(int mnozstvi) {
    m_mnozstvi += mnozstvi;
}

bool Surovina::snizMnozstvi(int mnozstvi) {
    if (m_mnozstvi < mnozstvi) return false;
    m_mnozstvi -= mnozstvi;
    return true;
}



void Surovina::printInfo() {
    cout << "Nazev zbozi: \t" << getNazev() << endl;
    cout << "Cena zbozi: \t" << getCena() << endl;
    cout << "Poplatek: \t" << getSkladovaciPoplatek() << endl;
    cout << "Mnozstvi na sklade: \t" << getMnozstvi() << endl;
}