//
// Created by micha on 19.12.2019.
//

#ifndef GAMECOMPANYMANAGER_JMENA_H
#define GAMECOMPANYMANAGER_JMENA_H

enum Jmena {
    Guma,
    Zelezo,
    Hlinik,
    Textil,
    Med,
    Superocel,
    Plastoid,
    Plasma,
    Karbonit,
    Klon,
    Hypermotor,

    BojovyDroidB1,
    Droideka,
    Stormtrooper,
    XWing,
    YWing,
    TieFighter,
    SithStarfighter,
    Jabitha,
    MillenniumFalcon,
    Devastator
};

#endif //GAMECOMPANYMANAGER_JMENA_H
