//
// Created by User on 09.11.2019.
//

#ifndef GAMECOMPANYMANAGER_SUROVINA_H
#define GAMECOMPANYMANAGER_SUROVINA_H

#include <iostream>
#include "Jmena.h"

using namespace std;


class Surovina {
protected:
    string m_nazev;
    int m_cena;
    int m_skladovaciPoplatek;
    int m_mnozstvi;
    Jmena m_jmeno;
public:
    Surovina(string nazev, int cena, int skladovaciPoplatek, Jmena jmeno);

    string getNazev();

    int getCena();

    int getSkladovaciPoplatek();

    int getMnozstvi();

    Jmena getJmeno();

    void zvysMnozstvi(int mnozstvi);

    bool snizMnozstvi(int mnozstvi);

    virtual void printInfo();

};


#endif //GAMECOMPANYMANAGER_SUROVINA_H
