//
// Created by micha on 21.11.2019.
//

#ifndef GAMECOMPANYMANAGER_SKLAD_H
#define GAMECOMPANYMANAGER_SKLAD_H

#include <vector>
#include <random>
#include "Zbozi.h"
#include "Surovina.h"
#include "Stroj.h"
#include "Factory.h"
#include "Jmena.h"

class Sklad {
    int m_kredity;
    vector<Zbozi *> m_zbozi;
    vector<Surovina *> m_suroviny;
    vector<Stroj *> m_vyrobniStroje;

    Sklad();

    void pridejSurovinu(Jmena jmeno, int mnozstvi, vector<PocetSurovin *> &pocetSurovin);

    void printOddelovac();

    int getCelkoveMnozstviZbozi();

    vector<Zbozi *> getDostupneZbozi();

    bool overDostatekSurovinKVyrobe(Zbozi *zbozi, int mnozstvi);

    bool overDostatekVyrobnihoCasu(Zbozi *zbozi, int mnozstvi);

public:

    static Sklad *createSklad();

    int getKredity();

    Surovina *najdiSurovinu(Jmena jmeno);

    Zbozi *najdiZbozi(Jmena jmeno);

    bool vyrobZbozi(Zbozi *zbozi, int mnozstvi);

    bool prodejSurovinu(Surovina *surovina, int mnozstvi);

    bool nakupSurovinu(Surovina *surovina, int mnozstvi);

    bool nakupStroj();

    bool prodejStroj();

    void resetStroje();

    void zpracujObjednavky();

    bool odectiPoplatky();

    void prictiKredity(int mnozstvi);

    bool odectiKredity(int mnozstvi);

    void printInfo();

    ~Sklad();

};


#endif //GAMECOMPANYMANAGER_SKLAD_H
