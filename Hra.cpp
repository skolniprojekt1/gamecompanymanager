//
// Created by User on 19.12.2019.
//

#include "Hra.h"

Hra::Hra() {
m_pocetKol = 1;
m_sklad = Sklad::createSklad();
}

void Hra::printTah() {
    cout << "---------------------------------------------" << endl;
    cout << "                   " << m_pocetKol << ". kolo" << endl;
    cout << "---------------------------------------------" << endl;
}

void Hra::printStart() {
    cout << "Vitej ve hre!" << endl;
    cout << "Tvym ukolem je vydelat co nejvice vesmirnych kreditu pri nakupech a prodejich vesmirnych lodi a droidu behem 10 kol." << endl;
    cout << "Pro zacatek mas k dispozici 50 000 vesmirnych kreditu a jeden stroj pro vyrobu zbozi. Delej co umis!" << endl;
    printTah();
}

void Hra::printInfo() {
    cout << "*** AKTUALNI STAV ***" << endl;
    m_sklad->printInfo();

}

void Hra::printVolby() {
    cout << "* 1 * NAKUP SUROVIN" << endl;
    cout << "* 2 * NAKUP STROJE" << endl;
    cout << "* 3 * PRODEJ STROJE" << endl;
    cout << "* 4 * VYROB ZBOZI" << endl;
    cout << "* 5 * VYPIS INFO"<< endl;
    cout << "* 6 * PRODEJ SUROVINU" << endl;
    cout << "* 7 * KONEC KOLA" << endl;

}

void Hra::hrej() {
    int volba;
    printStart();
    while (m_pocetKol < 10){
        printVolby();
        cin >> volba;
        int vyberSuroviny;
        int pocet;
        Surovina* surovina = nullptr;
        if (volba == 1 or volba == 6){
            cout << "Vyber surovinu cislem:"<< endl;
            cout << "1 - guma, 2 - zelezo, 3 - hlinik, 4 - textil, 5 - med" << endl;
            cout << "6 - superocel, 7 - plastoid, 8 = plasma, 9 - karbonit," << endl;
            cout << "10 - klon, 11 - hypermotor" << endl;
            cin >> vyberSuroviny;
            switch (vyberSuroviny){
                case 1: surovina = m_sklad->najdiSurovinu(Guma); break;
                case 2: surovina = m_sklad->najdiSurovinu(Zelezo); break;
                case 3: surovina = m_sklad->najdiSurovinu(Hlinik); break;
                case 4: surovina = m_sklad->najdiSurovinu(Textil); break;
                case 5: surovina = m_sklad->najdiSurovinu(Med); break;
                case 6: surovina = m_sklad->najdiSurovinu(Superocel); break;
                case 7: surovina = m_sklad->najdiSurovinu(Plastoid); break;
                case 8: surovina = m_sklad->najdiSurovinu(Plasma); break;
                case 9: surovina = m_sklad->najdiSurovinu(Karbonit); break;
                case 10: surovina = m_sklad->najdiSurovinu(Klon); break;
                case 11: surovina = m_sklad->najdiSurovinu(Hypermotor); break;
                default: cout << "Nevybrana zadna hodnota. Chybny vstup" << endl; break;
            }
            if (surovina == nullptr) continue;
            cout << "Napis pocet:"<< endl;
            cin >> pocet;
        }
        int vyberZbozi;
        Zbozi* zbozi = nullptr;
        if (volba == 4){
            cout << "Vyber zbozi cislem:"<< endl;
            cout << "1 - Bojovy droid B1, 2 - Droideka, 3 - Stormtrooper, 4 - Stihacka X-Wing, 5 - Y-Wing" << endl;
            cout << "6 - TIE Fighter, 7 - Sith Starfigter, 8 - Jabitha, 9 - Legendarni Millennium Falcon," << endl;
            cout << "10 - Devastator Lorda Vadera" << endl;
            cin >> vyberZbozi;
            switch (vyberZbozi){    // Pro vsechny pripady plati stejny prikaz, tzn. nepotrebujeme break u kazdeho
                case 1:             // a ke vstupu pripocita 10 -> vysledek se rovna pozici v enum
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10: zbozi = m_sklad->najdiZbozi(Jmena(vyberZbozi + 10)); break;
                default: cout << "Nevybrana zadna hodnota. Chybny vstup" << endl; break;
            }
            if (zbozi == nullptr) continue;
            cout << "Napis pocet:"<< endl;
            cin >> pocet;
        }
        bool konecHry = false;
        switch (volba){
            case 1:
                if (m_sklad->nakupSurovinu(surovina, pocet)) {
                    cout << "Koupeno" << endl;
                } else {
                    cout << "Nelze, nemate dostatek prostredku na koupi" << endl;
                }
                break;
            case 2:
                m_sklad->nakupStroj();
                break;
            case 3:
                m_sklad->prodejStroj();
                break;
            case 4:
                if(m_sklad->vyrobZbozi(zbozi, pocet)){
                    cout << "Vyrobeno" << endl;
                }else{
                    cout << "Nedostatek prostredku nebo vyrobniho casu" << endl;
                }
                break;
            case 5:
                m_sklad->printInfo();
                break;
            case 6:
                if (m_sklad->prodejSurovinu(surovina, pocet)) {
                    cout << "Prodano" << endl;
                } else {
                    cout << "Nelze, nemate dostatek surovin na prodej" << endl;
                }
                break;
            case 7:
                m_sklad->resetStroje();
                m_sklad->zpracujObjednavky();
                if (!m_sklad->odectiPoplatky()){
                    cout << "ZKRACHOVAL JSI! KONEC HRY." << endl;
                    konecHry = true;
                    break;
                }
                m_pocetKol++;
                printTah();
                printInfo();
                break;
            default:
                cout << "Nevybrana zadna hodnota. Chybny vstup." << endl;
                break;
        }
        if (konecHry) break;
        cin.clear();
        cin.ignore(9999999, '\n');
        volba = 0;
    }
    cout << "Ziskal jsi: " << m_sklad->getKredity() << " kreditu!" << endl;
}

Hra::~Hra() {
    delete m_sklad;
}
