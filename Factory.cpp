//
// Created by micha on 19.12.2019.
//

#include "Factory.h"


Stroj *Factory::createStroj() {
    return new Stroj(1000, 100000, 1000);
}

Surovina *Factory::createSurovina(Jmena jmeno) {
    switch (jmeno){
        case Guma:
            return new Surovina("Guma", 10, 1, Guma);
        case Zelezo:
            return new Surovina("Zelezo", 25, 2,Zelezo);
        case Hlinik:
            return new Surovina("Hlinik", 50, 2, Hlinik);
        case Textil:
            return new Surovina("Textil", 50, 2, Textil);
        case Med:
            return new Surovina("Med", 100, 5, Med);
        case Superocel:
            return new Surovina("Superocel", 100, 5, Superocel);
        case Plastoid:
            return new Surovina("Plastoid", 150, 10, Plastoid);
        case Plasma:
            return new Surovina("Plasma", 250, 10, Plasma);
        case Karbonit:
            return new Surovina("Karbonit", 700, 100, Karbonit);
        case Klon:
            return new Surovina("Klon", 18000, 1000, Klon);
        case Hypermotor:
            return new Surovina("Hypermotor", 100000, 1000, Hypermotor);
        default:
            return nullptr;
    }
}

Zbozi *Factory::createZbozi(Jmena jmeno, vector<PocetSurovin *> potrebneSuroviny) {
    switch (jmeno) {
        case BojovyDroidB1:
            return new Zbozi("Bojovy droid B1", 2160, 100, potrebneSuroviny, 10, BojovyDroidB1);
        case Droideka:
            return new Zbozi("Droideka", 4800, 200, potrebneSuroviny, 15, Droideka);
        case Stormtrooper:
            return new Zbozi("Stormtrooper", 25000, 800, potrebneSuroviny, 100, Stormtrooper);
        case XWing:
            return new Zbozi("Stihacka X-Wing", 120000, 1000, potrebneSuroviny, 500, XWing);
        case YWing:
            return new Zbozi("Y-Wing", 156000, 2000, potrebneSuroviny, 600, YWing);
        case TieFighter:
            return new Zbozi("TIE Fighter", 144000, 3000, potrebneSuroviny, 400, TieFighter);
        case SithStarfighter:
            return new Zbozi("Sith Starfighter", 192000, 4000, potrebneSuroviny, 550, SithStarfighter);
        case Jabitha:
            return new Zbozi("Jabitha", 280000, 2000, potrebneSuroviny, 1500, Jabitha);
        case MillenniumFalcon:
            return new Zbozi("Legendarni Millennium Falcon", 7120000, 50000, potrebneSuroviny, 10000, MillenniumFalcon);
        case Devastator:
            return new Zbozi("Devastator Lorda Vadera", 250000000, 500000000, potrebneSuroviny, 50000, Devastator);
        default:
            return nullptr;
    }
}
