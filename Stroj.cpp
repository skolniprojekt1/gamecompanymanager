//
// Created by micha on 21.11.2019.
//

#include "Stroj.h"

Stroj::Stroj(int vyrobniCas, int cena, int udrzba) : m_vychoziVyrobniCas(vyrobniCas) {
    m_vyrobniCas = vyrobniCas;
    m_udrzba = udrzba;
    m_cena = cena;
}

int Stroj::getVyrobniCas() {
    return m_vyrobniCas;
}

int Stroj::getCena() {
    return m_cena;
}

int Stroj::getUdrzba() {
    return m_udrzba;
}

bool Stroj::overCas(int cas) {
    return m_vyrobniCas >= cas;
}

void Stroj::snizCas(int cas) {
    if (overCas(cas)) m_vyrobniCas -= cas;
}

void Stroj::resetStroj() {
    m_vyrobniCas = m_vychoziVyrobniCas;
}

void Stroj::printInfo() {
    cout << "Cena stroje:\t" << getCena() << " kreditu" << endl;
    cout << "Maximalni pracovni doba stroje:\t" << m_vychoziVyrobniCas << " h" << endl;
    cout << "Zbyvajici pracovni doba stroje:\t" << getVyrobniCas() << " h" << endl;
    cout << "Cena za udrzbu:\t" << getUdrzba() << " kreditu" << endl;
}
