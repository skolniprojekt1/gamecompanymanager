//
// Created by micha on 21.11.2019.
//

#ifndef GAMECOMPANYMANAGER_STROJ_H
#define GAMECOMPANYMANAGER_STROJ_H

#include <iostream>

using namespace std;

class Stroj {
    const int m_vychoziVyrobniCas;
    int m_vyrobniCas;
    int m_cena;
    int m_udrzba;
public:
    Stroj(int vyrobniCas, int cena, int udrzba);

    int getVyrobniCas();

    int getCena();

    int getUdrzba();

    bool overCas(int cas);

    void snizCas(int cas);

    void resetStroj();

    void printInfo();
};


#endif //GAMECOMPANYMANAGER_STROJ_H
