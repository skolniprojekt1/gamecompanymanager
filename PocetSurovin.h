//
// Created by micha on 19.12.2019.
//

#ifndef GAMECOMPANYMANAGER_POCETSUROVIN_H
#define GAMECOMPANYMANAGER_POCETSUROVIN_H

#include "Surovina.h"

struct PocetSurovin {
    Surovina *surovina;
    int pocet;
};


#endif //GAMECOMPANYMANAGER_POCETSUROVIN_H
