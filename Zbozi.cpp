//
// Created by User on 18.12.2019.
//

#include "Zbozi.h"

Zbozi::Zbozi(string nazev, int cena, int skladovaciPoplatek, vector<PocetSurovin*> suroviny, int vyrobniCas,
             Jmena jmeno) : Surovina(nazev, cena, skladovaciPoplatek, jmeno) {
    m_potrebneSuroviny = suroviny;
    m_vyrobniCas = vyrobniCas;
}

vector<PocetSurovin*> Zbozi::getPotrebneSuroviny() {
    return m_potrebneSuroviny;
}

int Zbozi::getVyrobniCas() {
    return m_vyrobniCas;
}

void Zbozi::printInfo() {
    cout << "Nazev zbozi: \t" << getNazev() << endl;
    cout << "Prodejni cena: \t" << getCena() << " kreditu" << endl;
    cout << "Poplatek za skladovani za 1 kus: \t" << getSkladovaciPoplatek() << " kreditu" << endl;
    cout << "Mnozstvi na sklade: \t" << getMnozstvi() << " ks" << endl;
    cout << "Doba vyroby: \t" << getVyrobniCas() << " h" << endl;
    cout << "Suorviny potrebne k vyrobe 1 kusu a jejich pocet:" << endl;
    for (auto &surovinaAPocet : getPotrebneSuroviny()) {
        cout << "Surovina: " << surovinaAPocet->surovina->getNazev() << "\t Mnozstvi: " << surovinaAPocet->pocet << " kg" << endl;
    }
}

Zbozi::~Zbozi() {
    for (auto &potrebnaSurovina : m_potrebneSuroviny) delete potrebnaSurovina;
}
